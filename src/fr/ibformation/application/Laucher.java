package fr.ibformation.application;

import java.util.Scanner;

import fr.ibformation.application.objets.Archer;
import fr.ibformation.application.objets.Guerrier;
import fr.ibformation.application.objets.Hero;
import fr.ibformation.application.objets.Monstre;
import fr.ibformation.application.objets.Soigneur;

public class Laucher {

	public static void main(String[] args) {
		Monstre monstre = new Monstre(750);
		Archer archer = new Archer(400, "Legolas");
		Guerrier guerrier = new Guerrier(600, "Aragorn");
		Soigneur soigneur = new Soigneur(400, "Gandalf");
		Hero hero = new Hero(archer, guerrier, soigneur);

		while (!hero.getHeros().isEmpty() && monstre.getNombrePV() > 0) {
			monstre.subitDegatSaignement();
			actionMonstreGenererParOrdinateur(monstre, hero);
			
			// Attaque des h�ros
			if (!hero.getHeros().isEmpty()) {
				Scanner sc = new Scanner(System.in);
				for (int i = 0; i < hero.getHeros().size(); i++) {
					hero.getHeros().get(i).affichageChoixUtilisateur();
					int choixUtilisateur = sc.nextInt();
					if (hero.getHeros().get(i) instanceof Archer) {
						actionArcher(monstre, archer, choixUtilisateur);
					}
					if (hero.getHeros().get(i) instanceof Guerrier) {
						actionGuerrier(monstre, guerrier, choixUtilisateur);
					}
					if (hero.getHeros().get(i) instanceof Soigneur) {
						actionSoigneur(monstre, soigneur, choixUtilisateur);
					}
					
				}

			}
			System.out.println(hero);
			System.out.println(monstre);

		}

	}
	
	public static void actionSoigneur(Monstre monstre, Soigneur soigneur, int choixUtilisateur)
			throws IllegalArgumentException {
		switch (choixUtilisateur) {
		case 0: {
			break;
		}
		case 1: {
			soigneur.attaqueCibleeFaibleDegat(monstre);
			break;
		}
		case 2: {
			//guerrier.attaqueCibleefaibleDegatFaisantSaigner(monstre);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + choixUtilisateur);
		}
	}


	public static void actionGuerrier(Monstre monstre, Guerrier guerrier, int choixUtilisateur)
			throws IllegalArgumentException {
		switch (choixUtilisateur) {
		case 0: {
			break;
		}
		case 1: {
			guerrier.attaqueCibleeGrosDegat(monstre);
			break;
		}
		case 2: {
			guerrier.attaqueCibleefaibleDegatFaisantSaigner(monstre);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + choixUtilisateur);
		}
	}

	public static void actionArcher(Monstre monstre, Archer archer, int choixUtilisateur)
			throws IllegalArgumentException {
		switch (choixUtilisateur) {
		case 0: {
			break;
		}
		case 1: {
			archer.attaqueFaibleDeZone(monstre);
			break;
		}
		case 2: {
			archer.attaqueMoyenneCiblee(monstre);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + choixUtilisateur);
		}
	}

	public static void actionMonstreGenererParOrdinateur(Monstre monstre, Hero hero) throws IllegalArgumentException {
		if (monstre.getNombrePV() > 0) {
			int actionAleatoireMonstre = (int) (Math.random() * 2);
			switch (actionAleatoireMonstre) {
			case 0: {
				monstre.attaqueDeZoneMoyenDegat(hero);
				hero.removeSiMort();
				break;
			}
			case 1: {
				int cibleMonstre = (int) (Math.random() * hero.getHeros().size());
				monstre.attaqueCibleeEnormeDegat(hero.getHeros().get(cibleMonstre));
				hero.removeSiMort();
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + actionAleatoireMonstre);
			}
		}
	}

}
