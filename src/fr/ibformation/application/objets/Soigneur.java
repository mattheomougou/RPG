package fr.ibformation.application.objets;

public class Soigneur extends Personnage {
	private String name;
	public Soigneur() {
		// TODO Auto-generated constructor stub
	}
	public Soigneur(int nombrePV, String name) {
		super(nombrePV);
		this.name = name;
	}
	
	public void attaqueCibleeFaibleDegat(Monstre monstre) {
		monstre.subitDegat(Degat_faible);
	}
	
	public void SoinCibleeFaibleEfficacite(Personnage personnage) {
		personnage.setNombrePV(personnage.getNombrePV()+Degat_faible);
	}
	
	public void SoinCibleeMoyenneEfficacite(Personnage personnage) {
		if(personnage instanceof Monstre) {
			return;
		}
		personnage.setNombrePV(personnage.getNombrePV()+Degat_moyen);
	}
	
	public void SoinDeZoneSurLesPersonnagesAlli�s(Hero hero) {
		for(int i =0; i<hero.getHeros().size();i++) {
			SoinCibleeFaibleEfficacite(hero.getHeros().get(i));
		}
	}
	
	public void affichageChoixUtilisateur() {
		System.out.println("==========Choississez l'action que le soigneur doit faire=======");
		System.out.println("0 - Passer son tour");
		System.out.println("1 - Attaque de zone � faible d�g�ts(40) sur un personnage adverse");
		System.out.println("2 - Soin cibl� moyenne efficacit� sur un personnage alli�");
		System.out.println("3 - Soin de zone � faible efficacit� sur tout les personnages alli�s");
	}
	
	@Override
	public String toString() {
		return "Soigneur [name=" + name + ", nombrePV=" + nombrePV + "]";
	}
	protected String getName() {
		return name;
	}
	protected void setName(String name) {
		this.name = name;
	}
	
}
