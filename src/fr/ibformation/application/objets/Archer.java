package fr.ibformation.application.objets;

public class Archer extends Personnage {
	private String name;
	
	public Archer() {
		// TODO Auto-generated constructor stub
	}
	public Archer(int nombrePV, String name) {
		super(nombrePV);
		this.name = name;
	}
	
	public void attaqueFaibleDeZone(Monstre monstre) {
		monstre.subitDegat(Degat_faible);
	}
	
	public void attaqueMoyenneCiblee(Monstre monstre) {
		monstre.subitDegat(Degat_moyen);
	}
	
	public void subitDegat(int nombrePV) {
		int randomNombreEsquive = (int)Math.random()*4;
		if(randomNombreEsquive!=0) {
			this.setNombrePV(this.getNombrePV()-nombrePV);
		}
	}
	public void affichageChoixUtilisateur() {
		System.out.println("==========Choississez l'action que l'archer doit faire=======");
		System.out.println("0 - Passer son tour");
		System.out.println("1 - Attaque de zone � faible d�g�ts(40) sur les personnages adverses");
		System.out.println("2 - Attaque cibl�e � moyen d�g�ts(80) sur un personnage adverse");
	}
	
	@Override
	public String toString() {
		return "Archer [name=" + name + ", nombrePV=" + nombrePV + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
