package fr.ibformation.application.objets;

import java.util.ArrayList;
import java.util.List;

public class Hero  {
	private List<Personnage> heros;
	
	public Hero() {
		// TODO Auto-generated constructor stub
	}

	public Hero(Archer archer, Guerrier guerrier, Soigneur soigneur) {
		super();
		this.heros = new ArrayList<>(); 
		heros.add(archer);
		heros.add(guerrier);
		heros.add(soigneur);
	}
	
	public void removeSiMort() {
		for(int i =0;i<this.getHeros().size();i++) {
			if(this.getHeros().get(i).getNombrePV()==0) {
				this.heros.remove(i);
			}
		}
	}
	
	
	@Override
	public String toString() {
		return "Hero [heros=" + heros + ", toString()=" + super.toString() + "]";
	}

	public List<Personnage> getHeros() {
		return heros;
	}

	public void setHeros(List<Personnage> heros) {
		this.heros = heros;
	}
	
	

}
