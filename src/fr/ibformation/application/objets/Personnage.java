package fr.ibformation.application.objets;

public abstract class Personnage {
	protected int nombrePV;
	protected final int Degat_saignement =25;
	protected final int Degat_faible = 40;
	protected final int Degat_moyen = 80;
	protected final int Degat_eleve = 120;
	protected final int Degat_enorme = 160;

	public Personnage() {
		// TODO Auto-generated constructor stub
	}

	public Personnage(int nombrePV) {
		this.nombrePV = nombrePV;
	}
	
	@Override
	public String toString() {
		return "Personnage [nombrePV=" + nombrePV + "]";
	}

	public void SubitDegat(int nombrePV) {
		setNombrePV(getNombrePV()-nombrePV);
	}
	
	public void affichageChoixUtilisateur() {
		System.out.println("==========Choississez l'action que le personnage doit faire=======");
		System.out.println("0 - Passer son tour");
	}
	
	public int getNombrePV() {
		return nombrePV;
	}

	public void setNombrePV(int nombrePV) {
		if (nombrePV >= 0) {
			this.nombrePV = nombrePV;
		} else {
			this.nombrePV = 0;
		}
	}

}
