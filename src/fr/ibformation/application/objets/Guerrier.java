package fr.ibformation.application.objets;

public class Guerrier extends Personnage {
	private String name;
	public Guerrier() {
		// TODO Auto-generated constructor stub
	}
	public Guerrier(int nombrePV, String name) {
		super(nombrePV);
		this.name = name;
	}
	
	public void attaqueCibleeGrosDegat(Monstre monstre) {
		monstre.subitDegat(Degat_eleve);
	}
	
	public void attaqueCibleefaibleDegatFaisantSaigner(Monstre monstre) {
		monstre.subitDegat(Degat_faible);
		monstre.setBleeding(true);
	}
	
	public void SubitDegat(int nombrePV) {
		setNombrePV(getNombrePV()- ((int)(0.9*nombrePV)));
	}
	
	public void affichageChoixUtilisateur() {
		System.out.println("==========Choississez l'action que le guerrier doit faire=======");
		System.out.println("0 - Passer son tour");
		System.out.println("1 - Attaque cibl�e � gros d�g�ts(120) sur un personnage adverse");
		System.out.println("2 - Attaque cibl�e � faible d�g�ts(40) sur un personnage adverse et le faisant saigner (5% de sa vie/tour)");
	}
	
	
	@Override
	public String toString() {
		return "Guerrier [name=" + name + ", nombrePV=" + nombrePV + "]";
	}
	protected String getName() {
		return name;
	}
	
	protected void setName(String name) {
		this.name = name;
	}
	

}
