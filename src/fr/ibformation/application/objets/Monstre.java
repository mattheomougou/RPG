package fr.ibformation.application.objets;

public class Monstre extends Personnage {
	private boolean isBleeding;
	private boolean isResurrection;
	
	public Monstre() {
		// TODO Auto-generated constructor stub
	}
	public Monstre(int nombrePV) {
		super(nombrePV);
		isBleeding = false;
		isResurrection = false;
	}
	
	public void attaqueDeZoneMoyenDegat(Hero hero) {
		for(int i =0; i<hero.getHeros().size();i++) {
			attaqueCibleeMoyenDegat(hero.getHeros().get(i));
		}
	}
	
	public void attaqueCibleeMoyenDegat(Personnage personnage) {
		if(!(personnage instanceof Monstre)) {
			personnage.SubitDegat(Degat_moyen);
		}
	}
	
	public void attaqueCibleeEnormeDegat(Personnage personnage) {
		if(!(personnage instanceof Monstre)) {
			personnage.SubitDegat(Degat_enorme);
		}
	}
	
	
	protected boolean isBleeding() {
		return isBleeding;
	}
	protected void setBleeding(boolean isBleeding) {
		this.isBleeding = isBleeding;
	}
	protected boolean isResurrection() {
		return isResurrection;
	}
	protected void setResurrection(boolean isResurrection) {
		this.isResurrection = isResurrection;
	}
	@Override
	public String toString() {
		return "Monstre [isBleeding=" + isBleeding + ", isResurrection=" + isResurrection + ", toString()="
				+ super.toString() + "]";
	}
	public void subitDegat(int nombrePV) {
		this.setNombrePV(this.getNombrePV()-nombrePV);
	}
	
	public void subitDegatSaignement() {
		if(isBleeding) {
			subitDegat((int)(0.05*getNombrePV()));
		}
	}
	
	public void setNombrePV(int nombrePV) {
		if (nombrePV >= 0) {
			this.nombrePV = nombrePV;
		} else if(!isResurrection) {
			this.nombrePV = 750;
			isResurrection =true;
		}else {
			this.nombrePV=0;
		}
	}

}
